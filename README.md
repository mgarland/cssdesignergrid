# {css}designerGrid

### Way back when

This jQuery plugin was originally created back in 2010. The goal of {css}designerGrid was to assist web designers achieve pixel perfect design implementations using a grid layout system.  

Version 2.0.0 sees {css}designerGrid refined to work with jQuery 3.x versions. There are some glaring code inadequacies in the plugin but the plugin works, and is only designed to be used during development so these inadequacies should not hinder your ability to develop using {css}designerGrid.  

I have chosen to release version 2.0.0 as I believe it will still help web dev's achieve pixel perfect designs with browser vendors now supporting the css grid standard.

Cheers,

Mat.

