/********************************************************
 * jQuery Plugin: {css}designerGrid                     *
 * File name: designer-grid.js                          *
 * Version: 2.0.0                                       *
 * Date: May 29th, 2018                                 *
 *                                                      *
 * Mat Garland                                          *
 * e: matg74@gmail.com                                  *
 *                                                      *
 * Please contact me for any assistance or suggestions! *
 *                                                      *
 * Dual licensed under the MIT and GPL licenses:        *
 * http://www.opensource.org/licenses/mit-license.php   *
 * http://www.gnu.org/licenses/gpl.html                 *
 * Copyright (c) 2010 - 2018, Mathew Garland            *
 *                                                      *
 * There are still glaring issues with this code as it  *
 * was created way back in 2010, I have only just put   *
 * in the minimal effort to revive the plugin for       *
 * current versions of jQuery, this was fuelled by      *
 * browser vendors offering support for css grid.       *
 ********************************************************/
;(function ($) {
  $.fn.designerGrid = function (options) {
    let defaults = {
      docWidth: $(document).width(),
      docHeight: $(document).height(),
      opacity: 0.75,
      centred: true,
      colCount: 2,
      colColor: '#FF3',
      colWidth: 400,
      col_Z_index: -9999,
      grid_Z_index: -9999,
      colHeight: $(document).height(),
      gutter: 20,
      marginLeft: 100,
      marginTop: 20
    };
    let opts = $.extend(defaults, options);
    let startPos = 0;
    return this.each(function () {
      configureLayout(opts);
      let context = initCanvas('designer_grid');
      configureEvents(opts, context);
      setupGrid(context, opts);
      setupRulers(context, opts);
      setStartPos(opts);
      context = initCanvas('column_layout');
      drawColumns(context, opts);
      drawIndicators(context, opts);
    });

    function configureLayout(opts) {
      $(".wrapper").append('<canvas id="designer_grid" class="designer_grid" width="' + opts.docWidth + '" height="' + opts.docHeight + '"></canvas>');
      $('#designer_grid').css({'left': '0.5px'}).css({'top': '0.5px'}).css({'position': 'absolute'}).css({'overflow': 'hidden'}).css({'z-index': '' + opts.grid_Z_index + ''}).css({'filter': 'alpha(opacity(=' + opts.opacity * 100 + ')'}).css({'-moz-opacity': '' + opts.opacity + ''}).css({'-khtml-opacity': '' + opts.opacity + ''}).css({'opacity': '' + opts.opacity + ''});
      $(".wrapper").append('<canvas id="column_layout" width="' + opts.docWidth + '" height="' + opts.docHeight + '"></canvas>');
      $('#column_layout').css({'left': '0.5px'}).css({'top': '0.5px'}).css({'position': 'absolute'}).css({'overflow': 'hidden'}).css({'z-index': '' + opts.col_Z_index + ''}).css({'filter': 'alpha(opacity(=' + opts.opacity * 100 + ')'}).css({'-moz-opacity': '' + opts.opacity + ''}).css({'-khtml-opacity': '' + opts.opacity + ''}).css({'opacity': '' + opts.opacity + ''});
      $(".wrapper").append('<canvas id="coords" width= ' + opts.docWidth + ' height= ' + opts.docHeight + '></canvas>');
      $("#coords").css({'left': '0.5px'}).css({'top': '0.5px'}).css({'position': 'absolute'}).css({'overflow': 'hidden'}).css({'z-index': '-10000'}).css({'filter': 'alpha(opacity(=100)'}).css({'-moz-opacity': '1.0'}).css({'-khtml-opacity': '1.0'}).css({'opacity': '1.0'});
      $(".wrapper").append('<div class="designer_grid_status" align="center" width="400px" height="80px">' +
        '<div class="designer_title" width="25px" height="40px">{css}designerGrid v: 1.7.5</div' +
        '<br />W: ' + opts.docWidth + 'px | H: ' + opts.docHeight + 'px<div class="designer_grid_control" width="25px" height="40px">TOGGLE GRID [ON]/[OFF]</div>' +
        '<div class="designer_col_control" width="25px" height="40px">TOGGLE COLUMNS [ON]/[OFF]</div>' +
        '<div class="designer_index_control" width="25px" height="40px">TOGGLE GRID-Z-INDEX [+]/[-]</div></div>');

      $('.designer_grid_status').css({'position': 'absolute'}).css({'background-color': '#A0CFEC'}).css({'padding': '5px'}).css({'font': 'bold 10px sans-serif'}).css({'display': 'block'}).css({'left': opts.docWidth - 190}).css({'top': opts.docHeight - 120}).css({'z-index': '99999'}).css({'border': 'outset'}).css({'border-color': '#717D7D'});
      $('.designer_title').css({'background-color': '#717D7D'}).css({'margin-top': '5px'}).css({'padding-left': '5px'}).css({'padding-right': '5px'}).css({'font-size': '1.15em'}).css({'font-weight': 'bold'});
      $('.designer_grid_control').css({'cursor': 'pointer'}).css({'background-color': '#717D7D'}).css({'margin-top': '5px'}).css({'padding-left': '5px'}).css({'padding-right': '5px'});
      $('.designer_col_control').css({'cursor': 'pointer'}).css({'background-color': '#717D7D'}).css({'margin-top': '5px'}).css({'padding-left': '5px'}).css({'padding-right': '5px'});
      $('.designer_index_control').css({'cursor': 'pointer'}).css({'background-color': '#717D7D'}).css({'margin-top': '5px'}).css({'padding-left': '5px'}).css({'padding-right': '5px'});
    }

    function configureEvents(opts, context) {
      let resizeTimer = 0;
      $('.designer_grid_control').bind('click', function () {
        $('.designer_grid').is(':visible') ? $('.designer_grid').fadeOut() : $('.designer_grid').fadeIn();
      });
      $('.designer_col_control').bind('click', function () {
        $('#column_layout').is(':visible') ? $('#column_layout').fadeOut() : $('#column_layout').fadeIn();
      });
      $('.designer_index_control').bind('click', function () {
        $('.designer_grid').css('z-index') >= 1 ? $('.designer_grid').css({'z-index': '-9999'}) : $('.designer_grid').css({'z-index': '9999'});
      });
      $('.designer_grid').bind('mousedown', function (e) {
        let ctx = initCanvas('coords');
        ctx.strokeStyle = "#FF0000";
        ctx.fillStyle = "#FF0000";
        ctx.beginPath();
        ctx.moveTo(e.pageX, 0);
        ctx.lineTo(e.pageX, e.pageY);
        ctx.moveTo(0, e.pageY);
        ctx.lineTo(e.pageX, e.pageY);
        ctx.stroke();
        ctx.font = "10px arial";
        e.pageX <= 100 ? ctx.fillText('(' + e.pageX + ',' + e.pageY + ')', e.pageX + 10, e.pageY - 5) : ctx.fillText('(' + e.pageX + ',' + e.pageY + ')', e.pageX - 70, e.pageY - 5);
      });
      $('.designer_grid').bind('click', function (e) {
        let ctx = initCanvas('coords');
        ctx.clearRect(0, 0, opts.docWidth, opts.docHeight);
      });
      /*
          TODO:

          IMPLEMENT A WORKING WINDOW RESIZE EVENT WHICH SCALES THE GRID CONSTRUCTION BASED UPON RESPONSIVE DOCUMENT SIZE

       */
      $(window).resize(function(){
        /*clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
          let cvs = document.getElementById('designer_grid');
          context.clearRect(0, 0, cvs.width, cvs.height);
          let cvs2 = document.getElementById('column_layout');
          context.clearRect(0, 0, cvs2.width, cvs2.height);
          $.fn.designerGrid().call(this);
        }, 250);*/
      });
    }

    function initCanvas($obj) {
      let canvas = document.getElementById($obj);
      return canvas.getContext('2d');
    }

    function setupGrid(context, opts) {
      let end = opts.docWidth;
      context.beginPath();
      context.strokeStyle = "#eee";
      for (let x = 0.5; x < end; x += 20) {
        context.moveTo(x - 0.5, 0);
        context.lineTo(x - 0.5, end);
      }
      for (let y = 0.5; y < end; y += 20) {
        context.moveTo(0, y - 0.5);
        context.lineTo(end, y - 0.5);
      }
      context.stroke();
    }

    function setupRulers(context, opts) {
      let output;
      context.strokeStyle = "#000";
      context.fillStyle = "#000";
      context.font = "10px arial";
      for (let x = 0.5; x < opts.docWidth; x += 10) {
        context.beginPath();
        context.moveTo(x - 0.5, 0);
        if ((x - 0.5) % 100 === 0) {
          context.lineTo(x - 0.5, 10);
          context.stroke();
          output = '' + Math.floor(x) + '';
          context.fillText(output, x - 10, 17.5);
        } else {
          context.lineTo(x - 0.5, 5);
          context.stroke();
        }
      }
      context.strokeStyle = "#000";
      context.fillStyle = "#000";
      context.font = "10px arial";
      for (let y = 0.5; y < opts.docHeight; y += 10) {
        context.beginPath();
        context.moveTo(0, y - 0.5);
        if ((y - 0.5) % 100 === 0) {
          context.lineTo(10, y - 0.5);
          context.stroke();
          if (y > 100) {
            output = '' + Math.floor(y) + '';
            context.fillText(output, 12, y + 3.5);
          }
        }
        else {
          context.lineTo(5, y - 0.5);
          context.stroke();
        }
      }
    }

    function setStartPos(opts) {
      let gutterTotal = 0;
      if( opts.centred === true ) {
        opts.colCount >= 2 ? gutterTotal = ((opts.gutter * (opts.colCount - 1))) : gutterTotal = opts.gutter;
        startPos = (((opts.docWidth - ((opts.colWidth * opts.colCount) + gutterTotal)) / 2));
      } else {
        startPos = opts.marginLeft;
      }
    }

    function drawColumns(context, opts) {
      context.fillStyle = opts.colColor;
      for (let i = 1; i < opts.colCount + 1; i++) {
        switch (i) {
          case 1: {
            context.fillRect(startPos, opts.marginTop, opts.colWidth, opts.colHeight);
            break;
          }
          case 2: {
            context.fillRect((startPos + opts.colWidth + opts.gutter), opts.marginTop, opts.colWidth, opts.colHeight);
            break;
          }
          default: {
            context.fillRect((startPos + (opts.colWidth * (i - 1)) + (opts.gutter * (i - 1))), opts.marginTop, opts.colWidth, opts.colHeight);
            break;
          }
        }
      }
    }

    function drawIndicators(context, opts) {
      context.beginPath();
      context.strokeStyle = "#000";
      context.moveTo(0, opts.marginTop);
      context.lineTo(opts.docWidth, opts.marginTop);
      context.moveTo(opts.docWidth - 5, opts.marginTop - 5);
      context.lineTo(opts.docWidth, opts.marginTop);
      context.lineTo(opts.docWidth - 5, opts.marginTop + 5);
      context.moveTo(startPos, 0);
      context.lineTo(startPos, opts.docHeight);
      context.moveTo(startPos + 5, opts.docHeight - 5);
      context.lineTo(startPos, opts.docHeight);
      context.lineTo(startPos - 5, opts.docHeight - 5);
      context.stroke();
    }
  };
})(jQuery);
